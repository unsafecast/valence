#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <stdbool.h>

size_t char_to_integer(char character) {
    return character - '0';
}

typedef struct {
    char* str;
    size_t length;
} Str;

// Turns a null-terminated string into a Str, keeping the null terminator.
Str to_str(char* string) {
    return (Str) {
        .str = string,
        .length = strlen(string),
    };
}

// Returns true if the given string represents a base-10 integer, and can
// be converted into one.
bool str_is_integer(Str string) {
    for (size_t index = 0; index < string.length; index += 1) {
        if (!isdigit(string.str[index])) return false;
    }

    return true;
}


// Converts a Str into an integer. This only works if the Str is a valid
// representation of a base-10 integer (this can be checked using
// 'str_is_integer').
//
// If the string isn't a valid representation of a base-10 integer, it will
// return garbage.
size_t str_to_integer(Str string) {
    size_t total = 0;
    for (size_t index = 0; index < string.length; index += 1) {
        total = total * 10 + char_to_integer(string.str[index]);
    }

    return total;
}


// Returns true if both the lengths and the characters themselves are equal.
bool eq(Str left, Str right) {
    if (left.length != right.length) return false;
    for (size_t index = 0; index < left.length; index += 1) {
        if (left.str[index] != right.str[index]) return false;
    }

    return true;
}

void print(Str string) {
    fwrite(string.str, sizeof(char), string.length, stdout);
}

void println(Str string) { print(string); putc('\n', stdout); }


// Given a start and an end, the slice operation returns a new Str
// which contains the characters in between them.
//
// For a string "abcdefg", slice(str, 2, 4) will return "cde".
// This doesn't allocate, instead it uses the same string pointer.
Str slice(Str original, size_t start, size_t end) {
    return (Str) {
        .str = original.str + start,
        .length = end - start,
    };
}

typedef struct {
    size_t location;  // The index of the start of the token
    size_t length;
} Token;


// Returns the token's string value from the source code.
Str token_value(Token token, Str source) {
    return slice(source, token.location, token.location + token.length);
}

typedef struct {
    Str source;
    size_t location;
} Lexer;


// Did the lexer reach the end of the source code?
bool reached_end(const Lexer* lexer) {
    return lexer->location == lexer->source.length - 1;
}

// Returns the current character of the lexer.
char current(const Lexer* lexer) {
    return lexer->source.str[lexer->location];
}

Lexer create_lexer(Str source) {
    return (Lexer) {
        .source = source,
        .location = 0,
    };
}


// Advances the lexer's location to the next character.
// If you've reached the end, it does nothing.
void advance(Lexer* lexer) {
    if (!reached_end(lexer)) {
        lexer->location += 1;
    }
}


// Advances the lexer over the next token, and returns it.
Token lexer_next(Lexer* lexer) {
    while (isspace(current(lexer))) { advance(lexer); }

    size_t start = lexer->location;
    while (!isspace(current(lexer))) { advance(lexer); }
    return (Token) {
        .location = start,
        .length = lexer->location - start,
    };
}


// A generic intermediate representation of the source code.
//
// Optimizations and code generation to native code should be done using
// this format.
typedef struct {
    enum {
        Instruction_PushInt,
        Instruction_Call,
    } type;

    union {
        size_t PushInt;
        Str Call;
    } as;
} Instruction;

void print_instruction(Instruction instruction) {
    switch (instruction.type) {
        case Instruction_PushInt:
            printf("PushInt\t %zu", instruction.as.PushInt);
            break;
        case Instruction_Call:
            printf("Call\t ");
            print(instruction.as.Call);
            break;
    }
}

typedef struct {
    Instruction* buffer;
    size_t length;    // The number of instructions in the buffer

    size_t capacity;  // The number of instructions that can fit in the buffer
                      // without allocations.

    size_t step;  // How much the capacity will increase every time
                  // you try to push and run out of memory.
} InstructionBuffer;


// Creates an InstructionBuffer and allocates a capacity of 1 instruction.
InstructionBuffer create_instruction_buffer() {
    return (InstructionBuffer) {
        .buffer = malloc(sizeof(Instruction)),
        .length = 0,
        .capacity = 1,
        .step = 1,
    };
}


// Increases the buffer's capacity by 'count' and allocates memory
// to fit the capacity.
void reserve_instructions(InstructionBuffer* buffer, size_t count) {
    buffer->capacity += count;
    buffer->buffer = realloc(buffer->buffer,
                             sizeof(Instruction) * buffer->capacity);
}


// Appends an instruction to the buffer, increasing its capacity by 'step'
// if it runs out of memory.
void push_instruction(Instruction instruction, InstructionBuffer* buffer) {
    if (buffer->length + 1 >= buffer->capacity) {
        reserve_instructions(buffer, buffer->step);
    }

    buffer->buffer[buffer->length] = instruction;
    buffer->length += 1;
}

void print_instruction_buffer(const InstructionBuffer* buffer) {
    for (size_t index = 0; index < buffer->length; index += 1) {
        print_instruction(buffer->buffer[index]);
        putc('\n', stdout);
    }
}

// Turns source code directly into instructions
void generate_instructions(InstructionBuffer* buffer, Lexer* lexer) {
    while (true) {
        if (reached_end(lexer)) break;

        Token token = lexer_next(lexer);
        Str token_string = token_value(token, lexer->source);

        // If it's an integer, we push it.
        if (str_is_integer(token_string)) {
            Instruction instruction = (Instruction) {
                .type = Instruction_PushInt,
                .as.PushInt = str_to_integer(token_string),
            };
            push_instruction(instruction, buffer);
        }
        // Otherwise, we treat it as a word
        // TODO: Have some sort of context to create an error if a word
        //       doesn't exist
        else {
            Instruction instruction = (Instruction) {
                .type = Instruction_Call,
                .as.Call = token_string,
            };
            push_instruction(instruction, buffer);
        }
    }
}

int main() {
    Str source = to_str("34 35 + . ");
    Lexer lexer = create_lexer(source);

    InstructionBuffer buffer = create_instruction_buffer();
    buffer.step = 100;
    generate_instructions(&buffer, &lexer);
    print_instruction_buffer(&buffer);
}
